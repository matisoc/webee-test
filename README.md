# Webee Exam

## Before you begin

1.  Download and install the [Google Cloud
    SDK](https://cloud.google.com/sdk/docs/).
1.  [Install and configure Apache Maven](http://maven.apache.org/index.html).
1.  [Create a new Google Cloud Platform project, or use an existing
		one](https://console.cloud.google.com/project).
1.  Initialize the Cloud SDK.

        gcloud init

1.  Install the Cloud SDK `app-engine-java` component.

        gcloud components install app-engine-java

## Create and Deploying to App Engine

Set value of project id

	gcloud config set project VALUE

Authenticate in App Engine

	gcloud auth login

Create the app in App Engine
	
	gcloud app create
	
To deploy the app to App Engine, run, use the [Maven App Engine
plugin](https://cloud.google.com/appengine/docs/java/tools/using-maven).

    mvn clean appengine:deploy

After the deploy finishes, you can test REST API at
`https://YOUR_PROJECT.appspot.com/webee_exam/api/`, where `YOUR_PROJECT` is your Google Cloud
project ID.

## REST API Endpoints

### Insert a new device. Verify duplicity by mac address and timestamp format

    curl -i --header "Content-Type: application/json" --request POST --data '{"macAddress":"F2:B4:F0:E1:CC:D7","timeStamp":"2018-12-31 23:59:59"}' https://YOUR_PROJECT.appspot.com/webee_exam/api/push

### Get all existing devices in the datastore
    curl -i --header "Content-Type: application/json" --request GET https://YOUR_PROJECT.appspot.com/webee_exam/api/pull

### Get a device filtered by id
    curl -i --header "Content-Type: application/json" --request GET https://YOUR_PROJECT.appspot.com/webee_exam/api/pull/id/{id}

### Get a device filtered by mac address
    curl -i --header "Content-Type: application/json" --request GET https://YOUR_PROJECT.appspot.com/webee_exam/api/pull/mac/{mac}

## Demo

Demonstration of REST API in test application in App Engine (Project ID = webee-test-234119)

![](demo-min.gif)



