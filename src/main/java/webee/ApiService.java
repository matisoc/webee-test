package webee;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONException;

import webee.domain.Device;
import webee.service.GDataStoreService;
import webee.utils.Validator;
 
@Path("/api")
public class ApiService {
 
	final GDataStoreService gds = new GDataStoreService();
	
	/**
	 * Insert a new device in the datastore. 
	 * Verify duplicity and timestamp / mac address format
	 * @param a instance of webee.domain.Device (JSON or Empty)
	 * @return a instance of javax.ws.rs.core.Response (JSON or Empty)
	 */
	@Path("/push")
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response pushDevice(Device payload) throws JSONException {
		StringBuffer sb = new StringBuffer();
		boolean processOk = Validator.isValidTimeStamp(payload.getTimeStamp().trim(), sb) && Validator.isValidMac(payload.getMacAddress().trim(), sb);    	
	    if(processOk) gds.addDevice(payload, sb);
	    String json = "{\"Message\": \"" +sb.toString() + "\"}";
	    return Response.status(processOk? Response.Status.OK: Response.Status.BAD_REQUEST).entity(json).type(MediaType.APPLICATION_JSON).build();
	}
	
	/**
	 * Get all the devices that are in the datastore.
	 * @return a instance of javax.ws.rs.core.Response (Array of JSON or Empty)
	 */
	@Path("/pull")
	@GET
	@Produces("application/json")
	public Response pullAllDevices() throws JSONException {
		return Response.status(Response.Status.OK).entity(gds.getAllDevices().toArray()).type(MediaType.APPLICATION_JSON).build();
	}
	
	/**
	 * Get a device filtered by id found in the datastore. 
	 * @return a instance of javax.ws.rs.core.Response (JSON or Empty)
	 */
	@Path("/pull/id/{id}")
	@GET
	@Produces("application/json")
	public Response pullDeviceWithID(@PathParam("id") Long id) throws JSONException {
		if(id == null) throw new WebApplicationException(Response.Status.BAD_REQUEST);
		return Response.status(Response.Status.OK).entity(gds.getDevice(id, true)).type(MediaType.APPLICATION_JSON).build();
	}
	
	/**
	 * Get a device filtered by mac address found in the datastore. 
	 * @return @return a instance of javax.ws.rs.core.Response (JSON or Empty)
	 */
	@SuppressWarnings("null")
	@Path("/pull/mac/{mac}")
	@GET
	@Produces("application/json")
	public Response pullDeviceWithMAC(@PathParam("mac") String mac) throws JSONException {
		if(mac == null && !mac.isEmpty()) throw new WebApplicationException(Response.Status.BAD_REQUEST);
		return Response.status(200).entity(gds.getDevice(mac, false)).type(MediaType.APPLICATION_JSON).build();
	}
}
