package webee.utils;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Pattern;

public abstract class Validator {

	/** 
	 * Mac Address validator
	 * @param Mac Address in string format
	 * @param String builder of msg
	 * @return boolean
	 */
	public static boolean isValidMac(String mac, StringBuffer msg) { 
		String macPattern ="[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}";
		boolean ret = validatePattern(macPattern, mac);
		if(!ret) msg.append("Invalid MAC Address format. Should be 'FF:FF:FF:FF:FF:FF'. ");
        return ret;
    }
	
	/** 
	 * Timestamp validator
	 * @param Timestamp in string format
	 * @param String builder of msg
	 * @return boolean
	 */
	public static boolean isValidTimeStamp(String timeStamp, StringBuffer msg) {
		boolean ret = false;
		try{
			Timestamp ts = Timestamp.valueOf(LocalDateTime.parse( timeStamp , DateTimeFormatter.ofPattern( "yyyy-MM-dd HH:mm:ss", Locale.getDefault()) ));
			ret = ts.before(Timestamp.valueOf(LocalDateTime.of(2018, 1, 1, 0, 0, 0, 0)));
			if(ret) msg.append("Invalid timestamp value.");
		}catch(Exception e) {
			msg.append("Invalid timestamp format. Should be 'yyyy-MM-dd HH:mm:ss'. ");
		}
		return !ret;
    }
	
	/** 
	 * Matches validator
	 * @param Pattern in string format
	 * @param Value in string format
	 * @return boolean
	 */
	private static boolean validatePattern(String p, String str) {
		return Pattern.compile(p).matcher(str).matches();
	}
}
