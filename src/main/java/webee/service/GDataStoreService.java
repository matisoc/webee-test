package webee.service;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.EntityQuery.Builder;
import com.google.cloud.datastore.Key;
import com.google.cloud.datastore.KeyFactory;
import com.google.cloud.datastore.Query;
import com.google.cloud.datastore.StructuredQuery.PropertyFilter;

import webee.domain.Device;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class GDataStoreService {

  private static final String kind = "Device";
  private final Datastore datastore = DatastoreOptions.getDefaultInstance().getService();
  private final KeyFactory keyFactory = datastore.newKeyFactory().setKind(kind);
  
  /** 
	 * Add devices in to datastore
	 * @param a instance of Device
	 * @param String builder of msg
	 */
  public void addDevice(Device dev, StringBuffer sb) {
	Entity entity = pullDevice(dev.getMacAddress().trim(), false);
	if(entity == null) {
		Key key = datastore.allocateId(keyFactory.newKey());		
	    Entity deviceStore = Entity.newBuilder(key)
	        .set("macAddress", dev.getMacAddress().trim())
	        .set("timeStamp", dev.getTimeStamp())
	        .build();
	    datastore.put(deviceStore);
	    sb.append("Insert device ok. Id: " + deviceStore.getKey().getId());
	}
	else {
		sb.append("Device already existing in the datastore. ID: "+ entity.getKey().getId() );
	}
  }
  
  /** 
	 * Return device from datastore
	 * @param a instance of Object with value
	 * @param boolean used by filter
	 * @return a instance of webee.domain.Device
	 */
  public Device getDevice (Object value, boolean queryById) {
	  Entity entity = pullDevice(value,queryById);
	  if(entity == null) return null;
	  Device device = new Device();
	  device.setMacAddress((String)entity.getValue("macAddress").get());
	  device.setTimeStamp((String)entity.getValue("timeStamp").get());
	  return device;  
  }
  
  /** 
	 * Return all devices from dataastre
	 * @return Stream with instances of webee.domain.Device
	 */
  public Stream<Device> getAllDevices(){
	  return convertToDevice(pullAllDevices());
  }
  
  /** 
	 * Convert entity to Device
	 * @param Iterator of Entity instance
	 * @return Stream with instances of webee.domain.Device
	 */
  private Stream<Device> convertToDevice(Iterator<Entity> results){
	  List<Device> listDevices = new ArrayList<Device>();
	  while(results.hasNext()) {
		  Entity entity = results.next();
		  Device device = new Device();
		  device.setMacAddress((String)entity.getValue("macAddress").get());
		  device.setTimeStamp((String)entity.getValue("timeStamp").get());
		  listDevices.add(device);
	  }
	  return listDevices.stream();
  }
  
  /** 
	 * Get device from datastore
	 * @param a instance of Object with value
	 * @param boolean used by filter
	 * @return a instance of Entity
	 */
  private Entity pullDevice(Object value, boolean queryById) {
	  if(queryById) {
		  return datastore.get(keyFactory.newKey((Long)value));
	  }
	  else {
		  Map <String, Object []> params = new HashMap<String, Object[]>();
		  params.put("limit",new Object[] { new Integer(1), null});
		  params.put("filter",new Object[] { new String("macAddress"), ((String)value).trim()});
		  Iterator<Entity> results = queryDevices(params);
		  return results.hasNext() ? results.next() : null;
	  }	    
  }
  
  /** 
	 * Get alls device from datastore
	 * @return Iterator of Entity instances
	 */
  private Iterator<Entity> pullAllDevices() {
	    return queryDevices(null);
  }

  /** 
	 * Get Devices from datastore by queries
	 * @param Map with params in format k-v
	 * @return Iterator of Entity instances
	 */
  private Iterator<Entity> queryDevices(Map<String, Object[]> params) {
	  Builder builder = Query.newEntityQueryBuilder().setKind(kind);
	  if(params != null) {
		  params.forEach((k, v) -> {
			  if(k.equalsIgnoreCase("limit")) builder.setLimit(((Integer)v[0]));
			  else if(k.equalsIgnoreCase("filter")) builder.setFilter(PropertyFilter.eq(((String)v[0]), ((String)v[1])));
		  });  
	  }
	  Query<Entity> query = builder.build();
	  return datastore.run(query);
  }
}